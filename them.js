function renderSP(sp){
    var contentHTML = "";
    for ( var index= 0;index<sp.length;index++){
        var sanPha = sp[index];
        var contentTr= `<tr>
        <td>${sanPha.id}</td>
        <td>${sanPha.name}</td>
        <td>${sanPha.price}</td>
        <td>
        <img  src="${sanPha.img}" alt="ok" />
        
        </td>
        <td>${sanPha.desc}</td>
        <td>${sanPha.screen}</td>
        <td>${sanPha.frontCamera}</td>
        <td>${sanPha.backCamera}</td>
        <td>${sanPha.type}</td>
        <td>
  <button onclick=xoaSanP('${sanPha.id}') class= "btn btn-warning">Xoá</button>
  <button onclick=suaSanP('${sanPha.id}') 
  data-toggle="modal" data-target="#myModal"
  class= "btn btn-primary">Sửa</button>
  </td>
        </tr>`
        
        contentHTML+=contentTr;
    };
document.getElementById("tblDanhSachSP").innerHTML=contentHTML;
};
function layThongTinTuForm(){
    var sttSp = document.getElementById("sttSP").value;
    var tenSp = document.getElementById("TenSP").value;
    var giaSp = document.getElementById("GiaSP").value;
    var hinhSp = document.getElementById("HinhSP").value;
    var motaSp = document.getElementById("MoTa").value;

    var camT = document.getElementById("sttTruoc").value;
    var camS = document.getElementById("sttSau").value;
    var mangH = document.getElementById("sttMh").value;
    var loaiSp = document.getElementById("sttLoai").value;
    
  

    var sp = {
        id:sttSp,
        name: tenSp,
        price:giaSp,
        img:hinhSp,
        desc:motaSp,
        frontCamera:camT,
        backCamera:camS,
        screen:mangH,
        type:loaiSp,
    }
    return sp;
}
function showThongTinLenForm(sp){
    document.getElementById("sttSP").value = sp.id;
    document.getElementById("TenSP").value = sp.name;
    document.getElementById("GiaSP").value = sp.price;
    document.getElementById("HinhSP").value = sp.img;
    document.getElementById("MoTa").value = sp.desc;
    document.getElementById("sttTruoc").value = sp.frontCamera;
    document.getElementById("sttSau").value = sp.backCamera;
    document.getElementById("sttMh").value = sp.screen;
    document.getElementById("sttLoai").value = sp.type;

};
var validation = {
    kiemTraRong: function (value, idError, message) {
      if (value.length == 0) {
        document.getElementById(idError).innerText = message;
        return false;
      } else {
        document.getElementById(idError).innerText = "";
        return true;
      }
    },
    kiemTraDoDai:function (value, idError, message,min,max){
      if(value.length < min || value.length > max){
        document.getElementById(idError).innerText = message;
        return false;
      }else { document.getElementById(idError).innerText = "";
      return true;
      }
    },
    kiemTraChu: function (value, idError, message) {
      let re =  "^[a-zA-Z]+$";
      if (!value.match(re)) {
        document.getElementById(idError).innerText = message;
        return false;
      } else {
        document.getElementById(idError).innerText = "";
        return true;
      }
    },
}

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}