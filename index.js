const BASE_URL = "https://62eca9b155d2bd170e843669.mockapi.io"

function GetSP (){
  batLoading();
axios({
    url:`${BASE_URL}/nv`,
    method:"GET",
}).then(function(res){
  tatLoading();
    renderSP(res.data);
    console.log(res);
}).catch(function(err){  tatLoading();
  console.log(err);});
};
GetSP();

function themSP(){
    var sp = layThongTinTuForm();


    //validayter


    let isValid=validation.kiemTraRong(sp.id,"spanMaSV","Stt sản phẩm không được rỗng");
    isValid =
    isValid &
    validation.kiemTraRong(
      sp.name,
      "spanTen",
      "Tên sản phẩm không được rỗng"
    );
    isValid =
    isValid &
    validation.kiemTraRong(
      sp.price,
      "spanGia",
      "Giá sản phẩm không được rỗng"
    ) && validation.kiemTraDoDai( sp.price, "spanGia","Giá sản phẩm phải từ 3 tới 5 kí tự",3,5);

    isValid =
    isValid &
    validation.kiemTraRong(
      sp.img,
      "spanHinh",
      "Hình sản phẩm không được rỗng"
    );
    isValid =
    isValid &
    validation.kiemTraRong(
      sp.desc,
      "spanMo",
      "Mô tả sản phẩm không được rỗng"
    );
    isValid =
    isValid &
    validation.kiemTraRong(
      sp.frontCamera,
      "spanTruoc",
      "Camera Trước không được rỗng"
    );
    isValid =
    isValid &
    validation.kiemTraRong(
      sp.backCamera,
      "spanSau",
      "Camera Sau không được rỗng"
    );
    isValid =
    isValid &
    validation.kiemTraRong(
      sp.screen,
      "spanMh",
      "Màng hình không được rỗng"
    );
    isValid =
    isValid &
    validation.kiemTraRong(
      sp.type,
      "spanLoai",
      "Màng hình không được rỗng"
    );
    if(isValid){
   
    batLoading()
    axios({
        url:`${BASE_URL}/nv`,
        method: "POST",
        data: sp,
    }).then(function(res){
        console.log(res);
        tatLoading();
        GetSP();
    })
    .catch(function(err){  tatLoading();
      console.log(err);});
   }
}
function xoaSanP(id){
  batLoading();
    axios({
        url:`${BASE_URL}/nv/${id}`,
        method:"DELETE",
    }).then(function(res){
      tatLoading();
        console.log(res);
        GetSP();
    }).catch(function(err){
      tatLoading();
      console.log(err);});
};
function suaSanP(id){
    axios({
        url:`${BASE_URL}/nv/${id}`,
        method: "GET",
    }).then(function(res){ 
      console.log(res);
        showThongTinLenForm(res.data);
    })
    .catch(function(err){ 
      console.log(err);});
};
function capNhatSp(){
    var nv = layThongTinTuForm();
    batLoading();
    axios({
        url:`${BASE_URL}/nv/${nv.id}`,
        method: "PUT",
        data: nv,
    }).then(function(res){
      tatLoading();
        console.log(res);
        GetSP();
    })
    .catch(function(err){ tatLoading();
      console.log(err);});
 }


